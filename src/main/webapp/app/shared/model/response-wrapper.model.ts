import {HttpHeaders} from '@angular/common/http';
// import {Headers} from '@angular/common/http';

export class ResponseWrapper {
  constructor(
    public headers: Headers,
    public json: any,
    public status: number
  ) {}
}
