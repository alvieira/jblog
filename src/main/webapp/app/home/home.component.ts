import {Component, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {PostService} from '../entities/post';
import { Post } from '../shared/model/post.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: [
    'home.css'
  ]
})
export class HomeComponent implements OnInit {
  posts: Post[] = [];

  constructor(private postService: PostService) {}

  loadAll() {
    this.postService.query().subscribe(
      (res: HttpResponse<Post[]>) => {
        this.posts = res.body;
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    console.log('ngOnInit...');
    this.loadAll();
  }
}
